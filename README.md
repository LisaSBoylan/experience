This repository is a collection of "snippets" demonstrating the work experience of Lisa (Blakeney) Boylan.
The contents are not intended to be complete and have been edited to preserve proprietary information.
However, the contents should demonstrate expert use of:
	OO Design and Programming
	OO Design Patterns and algorithms
	Database design and programing
	Software Architecture
	System Architecture
	Project Management
	Backend development
	Full Stack development
	Research and Development