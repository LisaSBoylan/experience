function ParseUrl(queryString)
{

   var dict = {};

   // remove the leading question mark from the 
   // query string if it is present
   if (queryString.charAt(0) == '?') 
      queryString = queryString.substring(1);

   // check whether the query string is empty
   if (queryString.length > 0){

      // replace plus signs in the query string with spaces
      queryString = queryString.replace(/\+/g, ' ');

      // split the query string around 
      // ampersands and  semicolons
      var queryComponents = queryString.split(/[&;]/g);

      // loop over the query string components
      for (var index = 0;index < queryComponents.length;index ++){

         // extract this component's key-value pair
         var keyValuePair = queryComponents[index].split('=');
         var key          = decodeURIComponent(keyValuePair[0]);
         var value        = keyValuePair.length > 1
                          ? decodeURIComponent(keyValuePair[1]): '';


        // store the value
        dict[key] = value;

     }
	return(dict);
   }
}


chrome.webRequest.onBeforeRequest.addListener(function(details) {
    /*
     * 1a. Required: Regular expression 
     * corresponding to the starting script tag
     * OR for both start and end tags
     * Multi-line case insensitive
     * DO NOT change the variable name!
     */
    // const script_start_re = /<[\\x00-\\x1F\\x7F\\s]*script\\b[\\x00-\\x1F\\x7F|\\s]*[^>]*>.*<\\/[\\x00-\\x1F\\x7F|\\s]*script\\b[\\x00-\\x1F\\x7F|\\s]*>/gimu;
    const script_start_re= /<[\x00-\x1F\x7F\s]*script\b[\x00-\x1F\x7F|\s]*[^>]*>.*<\/[\x00-\x1F\x7F|\s]*script\b[\x00-\x1F\x7F|\s]*>/gi;    
    //const script_start_re= /<[\x00-\x1F\x7F\s]*script\b[\x00-\x1F\x7F|\s]*[^>]*>.*<\/.*script\b[\x00-\x1F\x7F|\s]*>/gi;

    /*
     * 1b. Optional: Regular expression 
     * corresponding to the ending script tag
     * Multi-line case insensitive
     * DO NOT change the variable name!
     * Leave the variable assigned to null 
     * if you are only using one regex
     */
    const script_end_re = null;
    let cancel = false;


    // TO DO: Add code to determine whether request should be handled by redirect() or below code
    
    if (details.method === "POST") 
    {
        // 2a. Required: Handle POST requests
        let formData = details.requestBody.formData;

        if(formData) 
        {    
           Object.keys(formData).forEach(key => 
           {
             formData[key].forEach(value => 
             {
               let res = script_start_re.test(value);
               if ( res !== false )
                   cancel = true;
             });
           });
        }else{
                   // 2b. Required: Handle GET requests
           let urlData = ParseUrl(details.url);
           if(urlData) 
           {
               for(let key in urlData) 
               {                
                    if(urlData.hasOwnProperty(key))
                    {
                      let cmpStr = urlData[key];
                      let res = script_start_re.test(cmpStr);
                      if ( res !== false )
                           cancel = true;
                    }
               }
           }
        }   
    }else if(details.method === "GET")
    {
        // 2b. Required: Handle GET requests
        let urlData = ParseUrl(details.url);        
		if(urlData)
		{
            for(let key in urlData) 
            {
        
                if(urlData.hasOwnProperty(key))
                {
                      let cmpStr = urlData[key];
                      let res = script_start_re.test(cmpStr);
                      if ( res !== false )
                           cancel = true;
                }        
            }
		}
    }
     return {cancel: cancel};
}, 
{
    urls: ["<all_urls>"]
}, ["blocking", "requestBody"]);
