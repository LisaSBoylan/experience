function getData()
{
    console.log("In GetData");
    
    // The data format from the remote site will be
    // "date": "YYYY-MM-DD",
    // "rates": {"CCC":dec,"CCC":dec,}, "base":"
    /* Use to test
    const data =
    {
        "date" : "2018-01-01",
        "rates":{"USD":1.0,"GBP":10.0},
        "base":"USD "        
    };
    */
    
    const data =
    {
        "date" : "2018-01-01",
        "rates":{"USD":1.0,"GBP":10.0},
        "base":"USD "        
    };
    /*
     * fetch("https://api.exchangeratesapi.io/latest?base=USD&symbols=USD,GBP,AUD,EUR")
     * then(response => response.jason())
     * then(data => {})
     *      console.log("Got Data");
     *      console.log(data);
     *  });
     **/
    console.log("Data:", data);
    render(data.rates);
}


function render(rates)
{
    console.log("In render");
    
    //Using rates, refresh the graph
         
    /*
    // The format of the HTML to be modified is:
    let graph = document.querySelect('#Graph');
    <div class="Graph">
        <div class="Graph-data1" onclick="alert(\'EUR costs 0.88lb\') " style="height:88.5%" ">EUR &#8364</div>
        <div class="Graph-data2" style="height:75.5%">USD $</div>
        <div class="Graph-data3" style="height:55.9%">AUD $</div>
        <div class="Graph-data4" style="height:45.5%">GBP &pound</div>
    </div>
    */
    
    // Get the rates
    // Note: used for..of becasue it is MUCH faster than Object.entries
    let lineNum = 1;
    
    for (const key of Object.keys(rates))
    {
        const value = rates[key];
        
        //Check for properties from prototype chain   
        if (rates.hasOwnProperty(key))
        {
            //not a property from prototype chain
            let graphString='.Graph-data' + lineNum++;

            // format value to be x.xx
            const formatValue = parseFloat(value).toFixed(2);
            // format the HTML style attribute
            const style = 'height:' + formatValue + '%';
            
            // Modify the DOM Element
            let graphBar = document.querySelector(graphString); 
            graphBar.setAttribute("style", style);
            graphBar.textContent=key;
        }
    }
}   