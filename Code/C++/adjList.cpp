#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>
#include <string.h>
#include <vector>
#include <stack>
#include <list>

std::vector<std::string> prologue;
std::vector<std::string> epilogue;

/*
 * C++ Program to Implement a Graph of Adjacency List
 * Based on understanding and research of VEX architecture, devise a scheduler for its VLIW instructions 
 * based on list scheduling and speculative load motion
 */

 
/*
 * Adjacency List of Edge
 */ 
struct AdjListEdge
{
    int            v_id;                // the id of the parent vertex
    std::string    s_reg;               // mark with dash is memory dependency
    std::string    d_type;              // data depencey type T= RAW F = WAW WAR:
    bool           visited;
    int            weight;              // In this case: latency
    struct         AdjListEdge* next;   // the child vertex
    struct         AdjListEdge* prev;   // the parent vertex
};
 

/*
 * Adjacency List 
 *     this is a array/vector like structure containing edges
 */  
struct AdjList
{
	struct AdjListEdge *head;
	struct AdjListEdge *tail;
};


struct Vertex
{
    int          v_id;                 //original instruction id/number
    int          num_dependencies;
    std::string  inst;
    std::string  opcode;
    bool         has_rhs_offset;
    bool         has_lhs_offset;
    std::string  s_reg;                // mark with dash if memory dependency
    bool         visited;
    std::vector<std::string>  rhs_operands;
    std::vector<std::string>  lhs_operands;
};


/*
 * Class Graph
 */ 
class Graph
{

    private:
	struct AdjList* array;
	std::vector<Vertex> vertices;
	std::vector<std::string> orig_inst;;   // these are the original 29 instructions corresponding to vertices but the first instruction is the "call" so orig_inst[1] = vertices[0]
	std::vector<int> roots;                // an array containing the id of a root Vertex

    public:
    Graph(std::vector<std::string> str)
    {
	    // Initialize each instruction node and push onto vertices vector
	    initVertex(str);  //intialize a Vertex and store to vertices
		// initialize the roots here-> 1st vertex = 1st root = 1st instruction 
        // Assuming it would be silly for 1st instruction to be a store
	    roots.push_back(vertices.front().v_id);
	    array= new AdjList[vertices.size()];  //could improve performance if link list or vector - but performance not requirement
	    for (int i = 0; i < vertices.size(); i++)
	    {
		    array[i].head = NULL;
		    array[i].tail = NULL;
    	}

	    for( int i=0; i< vertices.size(); i++)
			findEdge(&vertices[i]);

	    // Find Memory Dependencies
        for (int i = 0; i < roots.size(); ++i)
        {
		    AdjListEdge* pCrawl = array[i].head;
		    Vertex s;
            while (pCrawl)
            {
	            if (pCrawl->next != NULL)
                {
					s = vertices[pCrawl->next->v_id];
    		        if (isMemDependent( pCrawl->v_id, &s))
		            {
						pCrawl->s_reg="-";
						pCrawl->next->s_reg="-";
                    }
				}
				pCrawl = pCrawl->next;
            }
        }
    }

	/* 
	*	Initialiaize a Vertex
	*/
	void initVertex(std::vector<std::string> str)
	{
	    std::string tmp_str;
	    int inst_cnt = 0;
        // dont get call instruction
        std::string tmp = str[0];
	    orig_inst.push_back(str[0]);

        for(int i =1; i< str.size(); i++)
	    {
	        tmp_str = cleanStr(str[i]);
	        if (!tmp_str.empty())
	        {
				orig_inst.push_back(str[inst_cnt]);   //original instructions
				Vertex* v = new Vertex;
				v->v_id = inst_cnt;
				v->visited = false;
				v->inst = tmp_str;
				v->s_reg = "";
				v->has_lhs_offset = false;
				v->has_rhs_offset = false;
				setOpcode(v, tmp_str);
				setOperands(v,tmp_str);
	    	    v->num_dependencies = 0;
	    	    vertices.push_back(*v);
				inst_cnt++;
	        }
        }
	}


	std::string cleanStr(std::string str)
	{
		std::size_t begin=0;    
		std::size_t end=0;    
		std::string new_str;
		std::string rh_line;

        std::size_t pos = str.find("\tc0    ");
        if (pos == 0) 
	    {
            begin = str.find_first_not_of("\tc0    ");
            end = str.find_first_of("##");
			new_str = str.substr(begin,end-begin-3);
        } 
		return new_str;
	}

	/*
	  Parse the string obtain instruction operation
	*/
	void setOpcode(Vertex* v, std::string str)
	{
		// First word in string is the command/operation
		int e_begin;
		int e_end;
		
		e_begin = str.find_first_not_of(" ");
		e_end = str.find_first_of(" ");
		v->opcode= str.substr(e_begin, e_end);
	}

	/*
	    Parse the instruction to produce:
	    1. a list of operands on the right(rhs) hand side of the instruction = 
	    2. a list of operands on the left(lhs) hand side of the instruction = 
	    3. If a memory offset id found set the corresponding lhs or rhs offset indicator
	*/
	void setOperands(Vertex* v, std::string str)
	{
	    std::size_t p_begin;
	    std::size_t p_end;
	    std::string reg_name, lhs_string;

	    // Get just the instruction from the string
	    std::size_t pp_begin;
	    std::size_t pp_end;
	    std::string lh_inst;
	    std::string rh_inst;
	    
	    // Find = sign
	    p_begin = str.find("=");
	    p_begin = str.find_first_not_of("= ", p_begin);
	    if (p_begin != std::string::npos)
	    {
			// Process RHS
			p_end = str.find_first_of(",", p_begin); 

			// Test for Memory Assigmnet
			if (str.find("(",p_begin) != std::string::npos)
			{
				v->has_rhs_offset = true;
			}

			if (str.find("[", p_begin) != std::string::npos)
			{
				v->has_rhs_offset = true;
				p_end = str.find_first_of("[", p_begin); 
				p_begin = str.find_first_not_of(" ", p_begin);

				// Get Memory Offset
				reg_name=str.substr(p_begin, p_end-p_begin);

				if (!reg_name.empty())
				{
					v->rhs_operands.push_back(reg_name);
					reg_name.clear();
				}
				p_begin = str.find_first_not_of("[",p_end);
				p_end = str.find_last_of("]"); 
			}

			// process remaining rhs
			while ( p_end != std::string::npos)
			{

				// Get operand
				reg_name=str.substr(p_begin, p_end-p_begin);
				if (!reg_name.empty())
				{
					v->rhs_operands.push_back(reg_name);
					reg_name.clear();
				}
				p_begin = str.find_first_not_of(",]", p_end); 
				p_end = str.find_first_of(",", p_begin); 
			}
			
			if ( p_begin != std::string::npos)
			{
				p_begin = str.find_first_not_of(" ", p_begin);
				v->rhs_operands.push_back(str.substr(p_begin));
			}

			// Proccess LHS
			reg_name.clear();
			p_begin = str.find_first_of(" ");
			p_begin = str.find_first_not_of(" ", p_begin);
			p_end   = str.find("=");
			lhs_string = str.substr(p_begin, p_end-p_begin);
			p_end = lhs_string.find_last_not_of("= ", p_end);

			// Test for Memory Assigmnet
			lhs_string = str.substr(p_begin,p_end+1);  //lhs no nulls
			p_begin = 0;
			if (lhs_string.find("[") != std::string::npos)
			{
				v->has_lhs_offset = true;
				p_end = lhs_string.find_first_of("["); 

				// Get Memoory Offset
				reg_name=lhs_string.substr(p_begin, p_end);

				if (!reg_name.empty())
				{
					v->lhs_operands.push_back(reg_name);
					reg_name.clear();
				}
				lhs_string.erase(p_begin,p_end);
				p_begin = lhs_string.find_first_of("["); 
				p_end = lhs_string.find_first_of("]"); 
				reg_name=lhs_string.substr(1,p_end-1);
			} else
			{
				reg_name = lhs_string;	
			}

			if (!reg_name.empty())
			{
				v->lhs_operands.push_back(reg_name);
				reg_name.clear();
			}
	    }
	} // setOperands


	/*
        1. Determine Dependencies 
        2. Add edge foreach dependency
	*/
	void findEdge(Vertex* v)
	{
	    AdjListEdge* candidate;
	    int candidate_id;
	    std::string answer;
	    bool dependent =false;

        //special case - No roots resolve when Graph instantiated
        //Check each root dependency list
	    
	    for (int index=0; index < v->v_id ; index++)
	    {
	        //Get the dependency list for root[i]
	        candidate = getAdjListTail(index);

			if ((candidate != NULL) && (candidate->v_id != v->v_id))
			{
				// Check the leaf for dependency
	            answer = isDependent(candidate->v_id, v);
	            if (answer.compare("N") !=0)
				{	
					addEdge(index, v->v_id,answer);
					vertices[index].num_dependencies++;
					dependent =true;
				} 
				else  //check the previos node in the list (branch)
				{
                    if (candidate->prev != NULL )
					{
						candidate = candidate->prev;
	                    if ((candidate->v_id != v->v_id)) 
                        {
							answer= isDependent(candidate->v_id, v);
	                        if (answer.compare("N") !=0)
							{	
								//DEBUG add weight
								addEdge(index, v->v_id, answer);
								vertices[index].num_dependencies++;
								dependent =true;
							}
						}
					}
		       }
	        } else if (index != v->v_id) //No leaf, check array[i]
		    {
				// No leaf, check array[i]
				answer = isDependent(index, v);
	            if (answer.compare("N") !=0)
		        {
		            addEdge(index, v->v_id,answer);
		            vertices[index].num_dependencies++;
	    		    dependent =true;
                } 
	        }   
 	    }

 	    // If still no dependencies found add root
	    if ((!dependent) && (v->v_id != 0))  // dont compare to self
	    {  
	        roots.push_back(v->v_id);
        } 
    }

	/*
	  Return list head of dependencies for the given Vertex
    */
	AdjListEdge* getAdjList(int r)
	{
		return array[r].head;
	}

	/*
	  Return list tail of dependencies for the given Vertex
    */
	AdjListEdge* getAdjListTail(int r)
	{
		return array[r].tail;
	}


	/*
		Identify Dependencies
		Check for both Register and Memory Dependencies
		If there is a dependency, p would be the parent of v
		so determine is v is dependent upon p
	*/
    std::string isDependent(int p_id, Vertex* s)
	{
	    // v_id is the potential parent of s
		std::string answer;
		answer = isRegDependent(p_id,s);
		return answer;
	}

	std::string isRegDependent(int v_id , Vertex* s)
	{
		Vertex p = vertices[v_id];  //This is the would be predeccessor/parent
		//RAW
		for ( int i = 0; i<p.lhs_operands.size(); i++)
		{
			for (int j = 0; j< s->rhs_operands.size(); j++)
			{
				if ( (p.lhs_operands[i]).size() == (s->rhs_operands[j]).size() )
					if ( p.lhs_operands[i].compare( s->rhs_operands[j]) == 0)
						if (p.lhs_operands[i].find_first_of("xX")  == std::string::npos)
							if (s->rhs_operands[j].find_first_of("xX") == std::string::npos)
							{
								return "T";
							}
			}
		}

		//WAW
		for ( int i = 0; i<p.lhs_operands.size(); i++)
		{
			for (int j = 0; j< s->lhs_operands.size(); j++)
			{
				if ( (p.lhs_operands[i]).size() == (s->lhs_operands[j]).size())
					if ( p.lhs_operands[i].compare(s->lhs_operands[j])==0)
						if (p.lhs_operands[i].find_first_of("xX")  == std::string::npos)
							if (s->lhs_operands[j].find_first_of("xX") == std::string::npos)
							{
			                  return "F";
							}
			}
		}

		
		//WAR
		for ( int i = 0; i<p.rhs_operands.size(); i++)
		{
			for (int j = 0; j< s->lhs_operands.size(); j++)
			{
			  if ( (p.rhs_operands[i]).size() == (s->lhs_operands[j]).size())
			    if ( p.rhs_operands[i].compare(s->lhs_operands[j])==0)
			  //if ( strcmp ((p.rhs_operands[i]).c_str(), (s->lhs_operands[j]).c_str()) == 0 )
				if (p.rhs_operands[i].find_first_of("xX")  == std::string::npos)
                                  if (s->lhs_operands[j].find_first_of("xX") == std::string::npos)
				    {
			                  return "F";
				     }
			}
		}
		return "N";
	}

	bool isMemDependent(int v_id, Vertex* s)
	{
	    // v_id is the potential parent of s
		Vertex p = vertices[v_id];  //This is the would be predeccessor/parent

		// If p->opcode is a ldw, and s is a stw or mov, then == Dep
		// If p->opcode is a ldw to a register of s then == Dep
		// in case where a load or store may access a register creating a register dependency
	    // isRegDependent will catch 

		if (p.opcode.compare("ldw") == 0)
	    {   
			if ((s->opcode == "stw") || (s->opcode =="mov"))
			{ 
			    vertices[v_id].s_reg = "-";
			    vertices[s->v_id].s_reg = "-";
			    return true;
			}
		}		
	    return false;
	}

    /*
     * Creating New Adjacency List Edge
    */ 
    AdjListEdge* newAdjListEdge(int v_id, std::string d_type)
    {
        AdjListEdge* newEdge = new AdjListEdge;
        newEdge->v_id   = v_id;
        newEdge->s_reg  = ""; 
        newEdge->d_type= d_type;    
        newEdge->visited = false;
	    newEdge->weight = getWeight(vertices[v_id].opcode);
        newEdge->next   = NULL;
        newEdge->prev   = NULL;
        return newEdge;
    }

    /*
     * Adding Edge to Graph
    */ 
    void addEdge(int src, int v_id, std::string d_type)
    {
		AdjListEdge* newEdge= newAdjListEdge(v_id, d_type);
	    if (array[src].head == NULL)
	    {
			array[src].head = newEdge;
        }
        else
	    {
  	    	newEdge->prev = array[src].tail;
            array[src].tail->next = newEdge;
	    }
        array[src].tail = newEdge;
    }

	int getWeight(std::string str)
	{
		if (str.compare("mpylu") == 0)
			return 2;
		else if (str.compare("mpyhs") == 0)
			return 2;
		else if (str.compare("ldw") == 0)
			return 3;
		else if (str.compare("mov") == 0)
			return 4;
		else if (str.compare("call") == 0)
			return 1;
		else if (str.compare("return") == 0)
			return 1;
		else
			return 1;
	}

    /*
     * Print the graph
    */ 
    void printGraph()
    {
	    // roots should be same size as vertices
        for (int i = 0; i < roots.size(); ++i)
        {
			AdjListEdge* pCrawl = array[i].head;
            std::cout<<"\n Adjacency list of vertex "<< roots[i]<<"\n head ";
            while (pCrawl)
            {
                std::cout<<"-> "<<pCrawl->v_id;
                pCrawl = pCrawl->next;
            }
            std::cout<<"\n";
        }
    }

	void printEdge(AdjListEdge* e)
	{
		if (e != NULL)
		{
			std::cout << "Edge id = " << e->v_id << "\n";
			std::cout << "Edge weigth = " << e->weight << "\n";
			std::cout << "Edge s_reg->= " << e->s_reg << "\n";
			std::cout << "Edge d_type->= " << e->d_type << "\n";
			std::cout << "Edge visited->= " << e->visited << "\n";
			std::cout << "Edge next->= " << e->next << "\n";
			std::cout << "Edge prev->= " << e->prev << "\n";
		}
	}

	void printVertex(Vertex* v)
	{
		std::cout <<"PrintVertex v->v_id="<< v->v_id << "\n";
		std::cout <<"PrintVertex v->inst="<< v->inst << "\n";
		std::cout <<"PrintVertex v->has_rhs_offset="<< v->has_rhs_offset << "\n";
		std::cout <<"PrintVertex v->num_dependencies="<< v->num_dependencies << "\n";
		std::cout <<"PrintVertex v->has_lhs_offset="<< v->has_lhs_offset << "\n";
		std::cout <<"PrintVertex v->visited="<< v->visited << "\n";
		//std::cout <<"PrintVertex v->opcode="<< v->opcode << "\n";
		for ( int i = 0 ; i < v->lhs_operands.size(); i++)
			std::cout <<"PrintVertex v->lhs_operands[" << i << "]=" << v->lhs_operands[i] <<"|\n";
		for ( int i = 0 ; i < v->rhs_operands.size(); i++)
			std::cout <<"PrintVertex v->rhs_operands[" << i << "]=" << v->rhs_operands[i] << "|\n";

	}
};
 

/*
 * Main
 */ 

/*
int main( int argc, char **argv)
{
	std::vector<Vertex*> v_ptr;
	std::vector<std::string>  instructions;
	
	std::string s1 ="mpyhs 0x30[$r0.9] = $r0.2,$r0.5";
	instructions.push_back(s1);
	std::string s2 ="ldw $r0.5 = 0x30[$r0.1]";
	instructions.push_back(s2);
	std::string s3 ="mov $r0.3=(_?1STRINGPACKET.1 +0)";
	instructions.push_back(s3);
	std::string s4 ="call $10.0 = printf";
	instructions.push_back(s4);
	std::string s5 ="return $r0.1 = $r0.1,(0x49), $10.0";
	instructions.push_back(s5);

	Graph gh(instructions);
    	// print the adjacency list representation of the above graph
    	gh.printGraph();
 
    return 0;

}*/



/*
Read instructions from an input file and return all instructions   
*/
std::vector<std::string> readInput(const char* fname)
{
    std::string str;
    std::vector<std::string> instructions;
    
    std::ifstream filestream(fname);
    
    while (std::getline(filestream, str))
    {
        if (epilogue.empty()) {
            std::size_t pos = str.find("\tc0    ");
            if (pos == 0) {
                instructions.push_back(str);
            } else {
                pos = str.find(";;");
                if (pos == 0)
                    instructions.push_back(str);
                else {
                    pos = str.find(".call printf");
                    if (pos == 0) {
                        epilogue.push_back(str);
                    } else {
                        if (!instructions.empty()) {
							copy(instructions.begin(), instructions.end(), back_inserter(prologue));
                            instructions.clear();
                        }
                        prologue.push_back(str);
                    }
                }
            }
        } else {
            epilogue.push_back(str);
        }
    }
   
   return instructions;
}

/*
Print scheduled VLIW instructions to a file.
*/
void printOutput(const char* fname, std::vector<std::string> scheduledVLIW)
{
    std::ofstream outfile;
    outfile.open(fname);
  
    for (int i = 0; i < prologue.size(); i++)
        outfile << prologue[i] << "\n";

    for (int i = 0; i < scheduledVLIW.size(); i++)
        outfile << scheduledVLIW[i] << "\n";

    for (int i = 0; i < epilogue.size(); i++)
        outfile << epilogue[i] << "\n";

    outfile.close();
}

/*
TODO : Write any helper functions that you may need here. 

*/
int firstOccurence(int arr[], int n, int x)
{
    int first = -1;

    for (int i = 0; i < n; i++) {
      if (x != arr[i])
          continue;
      if (first == -1)
          first = i;
    }

    return first;
}

void printResource(std::vector<int> resource) {
    for (int g = 0; g < resource.size(); g++) {
    	std::cout << resource[g] << ' ';
    }
    std::cout << "\n";
}

std::vector<std::vector<int> > listScheduler(Graph graph, std::vector<int> orderedList)
{
  // ALU, ALU, ALU, ALU, MLU, MLU, MEMLOAD, MEMSTORE
  // 0 is available for use, other is to keep track of the cycle
  std::vector<std::vector<int> > resources;
  int rt_status[8] = {1, 1, 1, 1, 1, 1, 1, 1};
  // std::map<std::string, std::string> register_status = getRegisterTable();

  int cycle = 0;
  std::vector<int> visited;
  // Status instruction occupied
  std::vector<int> resource(8);
  std::vector<int> newList;

  for(int i = 0; i < orderedList.size(); i++){
  	// std::cout << orderedList[i] << "\n";
    newList.push_back(orderedList[i]);
  }

  for (int i = 0; i < resource.size(); i++) {
  	resource[i] = 0;
  }

  // std::vector<Vertex> readyList; 
  // std::vector<Vertex> inFlight;

  while(newList.size() > 0)  {
    // Single execution vs 4wide execution
    for (int i = 0; i < newList.size(); i++) {
	  	
	  	// adjust cycle
	    for (int c = 0; c < 8; c++) {
	    	if (rt_status[c] != 0) {
	      		rt_status[c] -= 1;

	      		// resource is available now
	      		if(rt_status[c] == 0)
	      			resource[c] = 0;
	    	}
    	}

      std::cout << graph.getVertex(newList[i]).opcode << "\n";
      if(std::find(visited.begin(), visited.end(), newList[i]) != visited.end()) {
        std::cout << "Visited vertex. Move to next vertex." << "\n";
      } else {
        // For first set of root nodes, set 
      	// std::cout << graph.getVertex(newList[i]).opcode << "\n";
        if (graph.getVertex(newList[i]).opcode == "add") {
          // Status for ALU
          // Check which resource is available between 0-3
          int idx = 0;
          int alu_status[4];
          
          for(int i = 0; i < 4; i++) {
          	alu_status[idx++] = rt_status[i];
          }
          // std::cout << alu_status[0] << alu_status[1] << "\n";
          int location = firstOccurence(alu_status, 4, 0);

          if(location == -1)
          	continue;

          // Check if resource is available
          if(location != -1) {
            rt_status[location] = 1;
            resource[location] = newList[i];
            newList.erase(newList.begin());
            // visited.push_back(newList[i]);
            // readyList[i] = readyList[i].getSuccessors();
          }

          printResource(resource);
        }

        if (graph.getVertex(newList[i]).opcode == "mpyhs" || graph.getVertex(newList[i]).opcode == "mpylu"){
          // Status for MLU
          // Check which resource is available and set between 4-5
          int idx = 0;
          int mlu_status[2];

          for(int i = 4; i < 6; i++) {
          	mlu_status[idx++] = rt_status[i];
          }
          // std::copy(rt_status + 5, rt_status + 6, mlu_status);
          int location = firstOccurence(mlu_status, 2, 0);

          if (location == -1)
          	continue;

          // Check if resource is available
          if(location != -1) {
            rt_status[location + 4] = 2;
            resource[location + 4] = newList[i];
            newList.erase(newList.begin());
            // visited.push_back(newList[i]);
            // readyList[i] = readyList[i].getSuccessors();
          }
        }

        if (graph.getVertex(newList[i]).opcode == "ldw") {
          int idx = 0;
          int mem_status[1];
          
          for(int i = 6; i < 7; i++) {
          	mem_status[idx++] = rt_status[i];
          }
          int location = firstOccurence(mem_status, 1, 0);

          if (location == -1) {
          	printResource(resource);
          	continue;
          }

          if(location != -1) {
            rt_status[location + 6] = 3;
            resource[location + 6] = newList[i];
            newList.erase(newList.begin());

            // visited.push_back(newList[i]);
            // readyList[i] = readyList[i].getSuccessors();
          }

          printResource(resource);
        }

        if (graph.getVertex(newList[i]).opcode == "stw") {
          int idx = 0;
          int mem_status[1];

          for(int i = 7; i < 8; i++) {
          	mem_status[idx++] = rt_status[i];
          }
          int location = firstOccurence(mem_status, 1, 0);

          if(location == -1)
          	continue;

          if(location != -1) {
            rt_status[location + 7] = 3;
            resource[location + 7] = newList[i];
            newList.erase(newList.begin());

            // visited.push_back(newList[i]);
            // readyList[i] = readyList[i].getSuccessors();
          }

          printResource(resource);
        }

        if (graph.getVertex(newList[i]).opcode == "mov") {

          int idx = 0;
          int mem_status[2];

          for(int i = 6; i < 8; i++) {
          	mem_status[idx++] = rt_status[i];
          }
          int location = firstOccurence(mem_status, 2, 0);

          if (location == -1)
          	continue;

          if(mem_status[0] == 0 && mem_status[1] == 0) {
          	rt_status[6] = 3;
          	rt_status[7] = 3;
            resource[6] = newList[i];
            resource[7] = newList[i];
            newList.erase(newList.begin());

            // visited.push_back(newList[i]);
            // readyList[i] = readyList[i].getSuccessors();
          }

          printResource(resource);
        }
        
        if (graph.getVertex(newList[i]).opcode == "call" || graph.getVertex(newList[i]).opcode == "all") {
 		  int idx = 0;
          int mem_status[2];

          for(int i = 6; i < 8; i++) {
          	mem_status[idx++] = rt_status[i];
          }
          // std::copy(rt_status + 6, rt_status + 7, mem_status);
          int location = firstOccurence(mem_status, 2, 0);

            if(mem_status[0] == 0 && mem_status[1] == 0) {
          	rt_status[6] = 3;
          	rt_status[7] = 3;
            resource[6] = newList[i];
            resource[7] = newList[i];
            newList.erase(newList.begin());

            // visited.push_back(newList[i]);
            // readyList[i] = readyList[i].getSuccessors();
          }
        }

      }
    }

    resources.push_back(resource);

    cycle += 1;

    // Adjust the cycles
    // for (int i = 0; i < 8; i++) {
    //   rt_status[i] -= 1;
    // }

    // for (int i = 0; i < inFlight.size(); i++) {
    //     if () {
    //       // remove op from inflight-list;
    //       // check nodes waiting for op and add to 
    //     }
    // }
  }

  return resources;
}

/*
Input : std::vector<std::string> instructions. The input is a vector of strings. Each string in the vector is an instruction in the original vex code.

Returns : std::vector<std::string>

The function should return a vector of strings. Each string should be a scheduled instruction or ;; that marks the end of a VLIW instruction word.
*/
std::vector<std::string>  scheduleVLIW(std::vector<std::string> instructions)
{
   std::vector<std::string> scheduledVLIW;

    Graph graph(instructions);

    // std::cout << "getTopoGraph method" << "\n";
    // getList(2);
    std::vector<int> orderedList = graph.getTopoGraph();

    // Vertex v = graph.getVertex(28);

    // std::cout << v.v_id << "\n";
    std::vector<std::vector<int> > table = listScheduler(graph, orderedList);

    // for(int i = 0; table.size(); i++) {
    // 	for(int j = 0; table[i].size(); j++) {
    // 		std::cout << table[i][j] << ' ';
    // 	}
    // 	std::cout << "\n";
    // }

    return scheduledVLIW;
} // class Graph

int main(int argc, char *argv[])
{

	if(argc != 2) 
	{
		std::cout << "Invalid parameters \n";
		std::cout << "Expected use ./vliwScheduler <input file name>\n";
	}
 
	const char* inputFile = argv[1];
	const char* vliwSchedulerOutput = "4_wide.s";

	std::vector<std::string> instructions;
	std::vector<std::string> scheduledVLIW;
 
	/* Read instructions from the file */
	instructions = readInput(inputFile);

	// Graph gh(instructions);
	// gh.printGraph();

	/* Schedule instructions */
	scheduledVLIW = scheduleVLIW(instructions);

	/* Print scheduled instructions to file */
	// printOutput(vliwSchedulerOutput, scheduledVLIW);
}




