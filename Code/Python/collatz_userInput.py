# -*- coding: utf-8 -*-
"""
Created on Thu Nov  3 03:46:46 2016

@author: Admin
"""


def collatz(number):
        if (number % 2) == 0:
            print(str(number) + ' // 2')
            return(number // 2)
        else:
            print('3 * ' + str(number) + ' + 1')
            return(3 * number + 1)
  

print('Please enter a number you would like me to collate')
while True:
    try:
        user_input=int(input())
        break;
    except ValueError:
        print('Please enter an integer value')

    
collate_count = 1
answer = collatz(user_input)

while ( answer != 1):
    print('The collate value is:' + str(answer))
    collate_count += 1
    answer = collatz(user_input)
    if collate_count > 100:       # Prevent infinte loop
        break

print('It took ' + str(collate_count) + ' times to colate to 1')