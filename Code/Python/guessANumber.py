# -*- coding: utf-8 -*-
"""
Created on Thu Nov  3 01:56:37 2016

@author: Admin
"""

import random

answer = random.randint(1,21)
guess = int(0)
guess_count = 0

while (guess != answer):
    guess_count += 1
    print('I am thinking of a number between 1 and 20')
    print('Take a guess')
    guess = int(input())
print('Good Job! You guessed my number in ' + str(guess_count) + ' guesses!')