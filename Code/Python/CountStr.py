# -*- coding: utf-8 -*-
"""
Created on Tue Nov  8 22:25:34 2016

@author: Lisa Boylan

Character Count
"""

import pprint

myString = 'Hello World'
stringCount={}

 
for i in myString:
    count = stringCount.setdefault(i,0)
    stringCount[i] = stringCount[i] + 1
    print('The value of ' + i + ' is ' + str(stringCount[i]))
print(stringCount)
print('This is pprint ')
pprint.pprint(stringCount)
print('This is pprint format')
print(pprint.pformat(stringCount)) #This prints the same as pprint.pprint()