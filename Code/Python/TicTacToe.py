# -*- coding: utf-8 -*-
"""
Created on Tue Nov  8 23:36:18 2016

@author: Admin
"""

theBoard = {'1':'_', '2':'_', '3':'_', '4':'_', '5':'_', '6':'_', '7':'_', '8':'_', '9':'_'}
            
       
def oMove(position):
        theBoard[position] = 'O'

def xMove(position):
        theBoard[position] = 'X' 
      
def printBoard():
    print('__' + theBoard['1'] + '__|' + '__' + theBoard['2'] + '__|' + '__' + theBoard['3'] + '__')
    print('__' + theBoard['4'] + '__|' + '__' + theBoard['5'] + '__|' + '__' + theBoard['6'] + '__')
    print('__' + theBoard['7'] + '__|' + '__' + theBoard['8'] + '__|' + '__' + theBoard['9'] + '__')
    
def checkTTT(token):
    # Horizontal TTT
    count = 0
    for i in ['1','2','3']:
        if theBoard[i] == token:
            count += 1
            if count == 3:
                return(True) 
        else:
            break

    count = 0            
    for i in ['4','5','6']:
        if theBoard[i] == token:
            count += 1
            if count == 3:
                return(True) 
        else:
            break
        
    count = 0        
    for i in ['7','8','9']:
        if theBoard[i] == token:
            count += 1
            if count == 3:
                return(True) 
        else:
            break
        
    # Vertical TTT
    count = 0
    for i in ['1','4','7']:
        if theBoard[i] == token:
            count +=1
            if count == 3:
                return(True) 
        else:
            break
     
    count = 0
    for i in ['2','5','8']:
        if theBoard[i] == token:
            count +=1
            if count == 3:
                return(True)
        else:
            break
        
    count = 0
    for i in ['3','6','9']:
        if theBoard[i] == token:
            count +=1
            if count == 3:
                return(True) 
        else:
            break
                
    # Diagonal TTT
    if theBoard['5'] == token:
        if theBoard['1'] == token:
            if theBoard['9'] == token:
                return True
        elif theBoard['3'] == token:
            if theBoard['7'] == token:
                return True
    return False

    
# Main Driver
print('O\'s will go first')
print('Enter a move (number 1 thru 9) or q to quit')
printBoard()

count = 1
token = 'O'

while '_' in theBoard.values():
    move = input()
    if move == 'q':
        break
    
    if (move not in ['1','2','3','4','5','6','7','8','9']) or (theBoard[move] != '_' ):
        print('I\'m am sorry, position: ' + str(move) + ' is taken.')
        print('Choose a number between 1 and 9, other than: ' + str(move))
        printBoard()
        continue
    else:
       count += 1
       if count % 2 == 0:
           token = 'O'
           oMove(move)
       else:
           token = 'X'
           xMove(move)
           
    if checkTTT(token):
        printBoard()
        print('Tick-Tac-Toe!   Congradulations player ' + token)
        break
    
    printBoard()
    if (count==10):
        print('It\'s a tie! Great Game...')

