# -*- coding: utf-8 -*-
"""
Created on Thu Nov  3 17:30:41 2016

@author: Admin
"""


cat_list1 = []
cat_list2 = []

while True:
    print('Please enter a cat name or stop to end the program')
    catName = input()
    if catName == 'stop':
        break
    if catName in cat_list1:
        print('You already entered that name')
    else:
        cat_list1 += [catName]
        cat_list2 += list(catName)

print('This is cat_list1')
for name in cat_list1:
    print('  ' + name)
print('This is cat_list2')   
for name in cat_list2:
    print('  ' + name)  
    
spam = ['cat', 'dog', 'bat']
spam.append('moose')
for name in spam:
    print('  ' + name)  
