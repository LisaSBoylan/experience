# -*- coding: utf-8 -*-
"""
Created on Fri Nov  4 21:25:53 2016

@author: Admin
"""
# replace list contents- reference
eggs = [1, 2, 3]
del eggs[2]
del eggs[1]
del eggs[0]
eggs.append(4)
eggs.append(5)
eggs.append(6)

print(eggs)

# Create a new list
eggs = [1, 2, 3]
eggs = [4, 5, 6]

print(eggs)