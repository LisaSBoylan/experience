# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

def isPhoneNumber(str):
    
    firstDigits=['2','3','4','5','6','7','8','9']
    
     # check str length = 15
    if len(str) != 12:
        return False
    
    # check 0-2 chars are neumbers 2-9
    for i in range(0,3):
        if str[i] not in firstDigits:
            return False

    # check to see if 4th and 8th char are -
    if str[3] != '-' or str[7] != '-':
        return False
        
    # 4-7, 8-12  chars are neumbers 0-9
    for i in range(4,7):
        if not str[i].isdecimal():
            return False
            
    for i in range(8,12):
        if not str[i].isdecimal():
            return False         
            
    return True

print('Is 415-555-4242 a phone number:')
print(isPhoneNumber('415-555-4242'))
print('Is Moshi moshi a phone number:')
print(isPhoneNumber('Moshi moshi'))