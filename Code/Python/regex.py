# -*- coding: utf-8 -*-
"""
Spyder Editor


    The ? matches zero or one of the preceding group.

    The * matches zero or more of the preceding group.

    The + matches one or more of the preceding group.

    The {n} matches exactly n of the preceding group.

    The {n,} matches n or more of the preceding group.

    The {,m} matches 0 to m of the preceding group.

    The {n,m} matches at least n and at most m of the preceding group.

    {n,m}? or *? or +? performs a nongreedy match of the preceding group.

    ^spam means the string must begin with spam.

    spam$ means the string must end with spam.

    The . matches any character, except newline characters.

    \d, \w, and \s match a digit, word, or space character, respectively.

    \D, \W, and \S match anything except a digit, word, or space character, respectively.

    [abc] matches any character between the brackets (such as a, b, or c).

    [^abc] matches any character that isn’t between the brackets
    (.*)   matches anything and everything except newline
    
    re.I ignore case    
    re.compile('.*', re.DOTALL): match all characters including newline


"""



#import regex (regular expression) compiler 
import re

def isPhoneNumber(searchStr):
    regexpattern = re.compile(r'\d{3}-\d{3}-\d{4}')

    mo = regexpattern.search(searchStr)
    if mo == None:
        return 'No phone number'
    else:
        return 'Yes. I found: ' + mo.group()

print('Is 415-555-4242 a phone number:')
print(isPhoneNumber('415-555-4242'))
print('Is Moshi moshi a phone number:')
print(isPhoneNumber('Moshi moshi'))
