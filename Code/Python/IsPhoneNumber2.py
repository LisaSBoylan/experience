# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#import regex (regular expression) compiler 
import re

def isPhoneNumber(searchStr):
    regexpattern = re.compile(r'\d{3}-\d{3}-\d{4}')

    mo = regexpattern.search(searchStr)
    print('Phone number found: ' + mo.group())

print('Is 415-555-4242 a phone number:')
print(isPhoneNumber('415-555-4242'))
print('Is Moshi moshi a phone number:')
print(isPhoneNumber('Moshi moshi'))