# -*- coding: utf-8 -*-
"""
Created on Sat Nov  5 01:32:58 2016

@author: Lisa Boylan

You can think of grid[x][y] (6,9)as being the character at the x- and y-coordinates
of a “picture” drawn with text characters. 
The (0, 0) origin will be in the upper-left corner, 
the x-coordinates increase going right, 
and the y-coordinates increase going down.

write code that uses it to print the image.

..OO.OO..
.OOOOOOO.
.OOOOOOO.
..OOOOO..
...OOO...
....O....

From  grid[0][0], to grid[8][5]


Remember:
__dict__ {}
list []
tuple ()

"""

grid = [['.', '.', '.', '.', '.', '.'],
        ['.', 'O', 'O', '.', '.', '.'],
        ['O', 'O', 'O', 'O', '.', '.'],
        ['O', 'O', 'O', 'O', 'O', '.'],
        ['.', 'O', 'O', 'O', 'O', 'O'],
        ['O', 'O', 'O', 'O', 'O', '.'],
        ['O', 'O', 'O', 'O', '.', '.'],
        ['.', 'O', 'O', '.', '.', '.'],
        ['.', '.', '.', '.', '.', '.']]
        
for x in range(6):
    for y in range(9):
          print(grid[y][x], end='')
    print("")
